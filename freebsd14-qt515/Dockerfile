FROM invent-registry.kde.org/sysadmin/ci-images/freebsd14-base
MAINTAINER KDE Sysadmin <sysadmin@kde.org>

# Ensure pkg is installed
RUN ASSUME_ALWAYS_YES=yes pkg update && pkg clean -a -y

# Disable default FreeBSD repository
RUN mkdir -p /usr/local/etc/pkg/repos/ && echo "FreeBSD { enabled: no }" > /usr/local/etc/pkg/repos/FreeBSD.conf

# Add our custom repository
ADD ci_pkgs.conf /usr/local/etc/pkg/repos/ci_pkgs.conf

# Refresh the package manager and start installing all our packages
RUN pkg update && pkg install --yes \
    accessibility/at-spi2-core \
    accessibility/qt5-speech \
    accessibility/speech-dispatcher \
    archivers/7-zip \
    archivers/brotli \
    archivers/gcab \
    archivers/libarchive \
    archivers/liblz4 \
    archivers/libmspack \
    archivers/libzip \
    archivers/lzo2 \
    archivers/minizip \
    archivers/quazip@qt5 \
    archivers/snappy \
    archivers/unzip \
    archivers/zip \
    archivers/zstd \
    astro/cfitsio \
    astro/stellarsolver \
    astro/wcslib \
    astro/xplanet \
    audio/alsa-lib \
    audio/alsa-plugins \
    audio/cdparanoia \
    audio/ebur128 \
    audio/espeak-ng \
    audio/faad \
    audio/flac \
    audio/flite \
    audio/fluidsynth \
    audio/gsm \
    audio/gstreamer1-plugins-a52dec \
    audio/gstreamer1-plugins-mpg123 \
    audio/gstreamer1-plugins-ogg \
    audio/gstreamer1-plugins-vorbis \
    audio/gtkpod \
    audio/id3lib \
    audio/jack \
    audio/lame \
    audio/liba52 \
    audio/libao \
    audio/libaudiofile \
    audio/libcanberra \
    audio/libcanberra-gtk3 \
    audio/libcddb \
    audio/libdiscid \
    audio/libgpod \
    audio/libid3tag \
    audio/liblastfm-qt5 \
    audio/libmad \
    audio/libmodplug \
    audio/libmusicbrainz5 \
    audio/libmysofa \
    audio/libogg \
    audio/libsamplerate \
    audio/libsndfile \
    audio/libsoxr \
    audio/libvorbis \
    audio/mpg123 \
    audio/openal-soft \
    audio/opus \
    audio/opusfile \
    audio/pocketsphinx \
    audio/portaudio \
    audio/pulseaudio \
    audio/pulseaudio-qt@qt5 \
    audio/sox \
    audio/speex \
    audio/speexdsp \
    audio/taglib \
    audio/twolame \
    audio/webrtc-audio-processing \
    chinese/libchewing \
    comms/libimobiledevice \
    comms/libusbmuxd \
    comms/qt5-connectivity \
    comms/qt5-sensors \
    comms/qt5-serialbus \
    comms/qt5-serialport \
    converters/fribidi \
    converters/libiconv \
    converters/p5-JSON \
    converters/p5-Text-Unidecode \
    converters/py-webencodings@py311 \
    databases/db5 \
    databases/freetds \
    databases/gdbm \
    databases/leveldb \
    databases/lmdb \
    databases/mariadb106-client \
    databases/mariadb106-server \
    databases/postgresql15-client \
    databases/postgresql15-server \
    databases/py-sqlite3@py311 \
    databases/qt5-sql \
    databases/qt5-sqldrivers-mysql \
    databases/qt5-sqldrivers-odbc \
    databases/qt5-sqldrivers-pgsql \
    databases/qt5-sqldrivers-sqlite2 \
    databases/qt5-sqldrivers-sqlite3 \
    databases/qt5-sqldrivers-tds \
    databases/sqlcipher \
    databases/sqlite2 \
    databases/sqlite3@default \
    databases/unixODBC \
    databases/xapian-core \
    deskutils/maliit-framework \
    deskutils/presage \
    deskutils/qtfeedback \
    devel/appstream-glib \
    devel/appstream-qt@qt5 \
    devel/apr1 \
    devel/atf \
    devel/autoconf \
    devel/autoconf-switch \
    devel/autoconf2.13 \
    devel/automake \
    devel/aws-sdk-cpp \
    devel/binutils \
    devel/bison \
    devel/boehm-gc \
    devel/boost-jam \
    devel/boost-libs \
    devel/build \
    devel/capstone4 \
    devel/catch2 \
    devel/ccache \
    devel/check \
    devel/cmake \
    devel/ctags \
    devel/cvsps \
    devel/dbus \
    devel/dbus-glib \
    devel/dconf \
    devel/desktop-file-utils \
    devel/dotconf \
    devel/double-conversion \
    devel/doxygen \
    devel/elfutils \
    devel/evdev-proto \
    devel/flatbuffers \
    devel/gconf2 \
    devel/gdb \
    devel/gettext \
    devel/gettext-runtime \
    devel/gettext-tools \
    devel/gflags \
    devel/git-lfs \
    devel/git@default \
    devel/gitlab-runner \
    devel/glib20 \
    devel/glog \
    devel/gmake \
    devel/gnulibiberty \
    devel/gobject-introspection \
    devel/googletest \
    devel/gperf \
    devel/grantlee5 \
    devel/gsettings-desktop-schemas \
    devel/icu \
    devel/indi \
    devel/jam \
    devel/json-glib \
    devel/jsoncpp \
    devel/kyua \
    devel/libIDL \
    devel/libatomic_ops \
    devel/libcutl \
    devel/libdaemon \
    devel/libdbusmenu-qt@qt5 \
    devel/libdevq \
    devel/libedit \
    devel/libepoll-shim \
    devel/libevdev \
    devel/libevent \
    devel/libffi \
    devel/libgit2 \
    devel/libglade2 \
    devel/libgsf \
    devel/libgudev \
    devel/libical \
    devel/libinotify \
    devel/libltdl \
    devel/libmtdev \
    devel/libnotify \
    devel/liboil \
    devel/libpci \
    devel/libpciaccess \
    devel/libphonenumber \
    devel/libplist \
    devel/libsoup \
    devel/libtextstyle \
    devel/libtool \
    devel/libudev-devd \
    devel/libunistring \
    devel/libunwind \
    devel/libuv \
    devel/libvolume_id \
    devel/llvm13 \
    devel/lldb-mi \
    devel/lutok \
    devel/m4 \
    devel/meson@py311 \
    devel/microsoft-gsl \
    devel/nasm \
    devel/ncurses \
    devel/ninja \
    devel/npth \
    devel/nspr \
    devel/onetbb \
    devel/oniguruma \
    devel/opencl \
    devel/orc \
    devel/p5-AppConfig \
    devel/p5-Locale-gettext \
    devel/p5-Locale-libintl \
    devel/p5-Path-Tiny \
    devel/p5-Term-ReadKey \
    devel/p5-Test-Needs \
    devel/p5-subversion \
    devel/pam_wrapper \
    devel/pcre \
    devel/pcre2 \
    devel/pkgconf \
    devel/popt \
    devel/protobuf \
    devel/py-Automat@py311 \
    devel/py-Jinja2@py311 \
    devel/py-apipkg@py311 \
    devel/py-asn1crypto@py311 \
    devel/py-atomicwrites@py311 \
    devel/py-attrs@py311 \
    devel/py-babel@py311 \
    devel/py-backports@py311 \
    devel/py-cffi@py311 \
    devel/py-chai@py311 \
    devel/py-check-jsonschema@py311  \
    devel/py-click@py311 \
    devel/py-configparser@py311 \
    devel/py-constantly@py311 \
    devel/py-contextlib2@py311 \
    devel/py-coverage@py311 \
    devel/py-cycler@py311 \
    py311-python-dateutil \
    devel/py-dbus@py311 \
    devel/py-evdev@py311 \
    devel/py-freezegun@py311 \
    devel/py-funcsigs@py311 \
    devel/py-future@py311 \
    devel/py-gobject3@py311 \
    devel/py-gyp@py311 \
    devel/py-hypothesis@py311 \
    devel/py-importlib-metadata@py311 \
    devel/py-incremental@py311 \
    devel/py-iso8601@py311 \
    devel/py-lxml@py311 \
    devel/py-mock@py311 \
    devel/py-more-itertools@py311 \
    devel/py-nose@py311 \
    devel/py-pathlib2@py311 \
    devel/py-pbr@py311 \
    devel/py-pip@py311 \
    devel/py-pluggy@py311 \
    devel/py-pretend@py311 \
    devel/py-py@py311 \
    devel/py-pyasn1@py311 \
    devel/py-pycparser@py311 \
    devel/py-pygdbmi@py311 \
    devel/py-pyparsing@py311 \
    devel/py-pytest-capturelog@py311 \
    devel/py-pytest-forked@py311 \
    devel/py-pytest-mock@py311 \
    devel/py-pytest-runner@py311 \
    devel/py-pytest-timeout@py311 \
    devel/py-pytest-xdist@py311 \
    devel/py-pytest@py311 \
    devel/py-python-gitlab@py311 \
    devel/py-pytz@py311 \
    devel/py-pyudev@py311 \
    devel/py-qt5 \
    devel/py-scripttest@py311 \
    devel/py-setuptools@py311 \
    devel/py-setuptools-scm@py311 \
    devel/py-simplejson@py311 \
    devel/py-sip@py311 \
    devel/py-six@py311 \
    devel/py-sortedcontainers@py311 \
    devel/py-twisted@py311 \
    devel/py-virtualenv@py311 \
    devel/py-wcwidth@py311 \
    devel/py-pyyaml@py311 \
    devel/py-zipp@py311 \
    devel/py-zope.interface@py311 \
    devel/pydbus-common \
    devel/pygobject3-common \
    devel/pyside2@py311 \
    devel/qca@qt5 \
    devel/qcoro@qt5 \
    devel/qscintilla2-qt5 \
    devel/qt5 \
    devel/qt5-assistant \
    devel/qt5-buildtools \
    devel/qt5-concurrent \
    devel/qt5-core \
    devel/qt5-dbus \
    devel/qt5-designer \
    devel/qt5-help \
    devel/qt5-linguist \
    devel/qt5-linguisttools \
    devel/qt5-location \
    devel/qt5-qdbus \
    devel/qt5-qdbusviewer \
    devel/qt5-qdoc \
    devel/qt5-qdoc-data \
    devel/qt5-qmake \
    devel/qt5-remoteobjects \
    devel/qt5-script \
    devel/qt5-scripttools \
    devel/qt5-scxml \
    devel/qt5-testlib \
    devel/qt5-uitools \
    devel/re2 \
    devel/readline \
    devel/rinutils \
    devel/rttr \
    devel/ruby-gems \
    devel/rubygem-power_assert \
    devel/rubygem-test-unit \
    devel/scons \
    devel/sdl12 \
    devel/sdl20 \
    devel/subversion \
    devel/t1lib \
    devel/tex-kpathsea \
    devel/tex-web2c \
    devel/vc \
    devel/woff2 \
    devel/xdg-user-dirs \
    devel/xdg-utils \
    devel/xorg-macros \
    devel/xsd \
    devel/yajl \
    devel/yasm \
    devel/zziplib \
    dns/c-ares \
    dns/libidn \
    dns/libidn2 \
    dns/py-idna@py311 \
    editors/editorconfig-core-c \
    editors/nano \
    editors/vim@console \
    emulators/tpm-emulator \
    finance/aqbanking \
    finance/libofx \
    ftp/curl \
    games/black-hole-solver \
    games/freecell-solver \
    german/hunspell \
    graphics/ImageMagick7@nox11 \
    graphics/aalib \
    graphics/argyllcms \
    graphics/cairo \
    graphics/colord \
    graphics/djvulibre \
    graphics/exiv2 \
    graphics/freeglut \
    graphics/frei0r \
    graphics/gd \
    graphics/gdk-pixbuf2 \
    graphics/giflib \
    graphics/glew \
    graphics/graphite2 \
    graphics/graphviz \
    graphics/gstreamer1-plugins-png \
    graphics/gtk-update-icon-cache \
    graphics/imlib2 \
    graphics/jasper \
    graphics/jbig2dec \
    graphics/jbigkit \
    graphics/jpeg-turbo \
    graphics/kcolorpicker@qt5 \
    graphics/kimageannotator@qt5 \
    graphics/kseexpr \
    graphics/lcms \
    graphics/lcms2 \
    graphics/lensfun \
    graphics/leptonica \
    graphics/libGLU \
    graphics/libavif \
    graphics/libdmtx \
    graphics/libdrm \
    graphics/libepoxy \
    graphics/libetonyek01 \
    graphics/libexif \
    graphics/libfpx \
    graphics/libglvnd \
    graphics/libgphoto2 \
    graphics/libheif \
    graphics/libjxl \
    graphics/libjxr \
    graphics/liblqr-1 \
    graphics/libmng \
    graphics/libmypaint \
    graphics/libpgf \
    graphics/libplacebo \
    graphics/libpotrace \
    graphics/libqrencode \
    graphics/libraw \
    graphics/librsvg2-rust \
    graphics/libwmf@nox11 \
    graphics/libwpg03 \
    graphics/mesa-demos \
    graphics/mesa-dri \
    graphics/mesa-libs \
    graphics/movit \
    graphics/netpbm \
    graphics/opencolorio \
    graphics/opencv \
    graphics/openexr \
    graphics/openjpeg \
    graphics/openjpeg15 \
    graphics/p5-Image-ExifTool \
    graphics/png \
    graphics/podofo \
    graphics/poppler \
    graphics/poppler-data \
    graphics/poppler-qt5 \
    graphics/poppler-utils \
    graphics/pstoedit \
    graphics/py-cairo@py311 \
    graphics/py-imagesize@py311 \
    graphics/qt5-3d \
    graphics/qt5-graphicaleffects \
    graphics/qt5-imageformats \
    graphics/qt5-opengl \
    graphics/qt5-pixeltool \
    graphics/qt5-svg \
    graphics/qt5-wayland \
    graphics/s2tc \
    graphics/sane-backends \
    graphics/sdl2_image \
    graphics/sdl_image \
    graphics/shaderc \
    graphics/svgalib \
    graphics/tesseract \
    graphics/tesseract-data \
    graphics/tiff \
    graphics/vulkan-headers \
    graphics/vulkan-loader \
    graphics/vulkan-tools \
    graphics/wayland \
    graphics/wayland-protocols \
    graphics/wayland-utils \
    graphics/webp \
    hebrew/hspell \
    japanese/anthy \
    java/javavmwrapper \
    java/openjdk8 \
    lang/cython@py311 \
    lang/expect \
    lang/f2c \
    lang/lua52 \
    lang/lua53 \
    lang/ocaml \
    lang/p5-Error \
    lang/python2 \
    lang/python27 \
    lang/python3 \
    lang/python39 \
    lang/ruby31 \
    lang/rust \
    lang/spidermonkey78 \
    lang/tcl86 \
    lang/vala \
    mail/dovecot \
    mail/opensmtpd \
    mail/opensmtpd-extras-table-postgresql \
    mail/spamd \
    math/Imath \
    math/R \
    math/blas \
    math/cblas \
    math/cln \
    math/eigen3 \
    math/facile \
    math/fftw3 \
    math/fftw3-float \
    math/gmp \
    math/gnuplot-lite \
    math/gsl \
    math/lapack \
    math/libqalculate \
    math/maxima \
    math/metis \
    math/mpc \
    math/mpfr \
    math/octave \
    math/openblas \
    math/py-kiwisolver@py311 \
    math/py-matplotlib@py311 \
    math/py-numpy@py311 \
    math/py-random2@py311 \
    math/suitesparse \
    math/z3 \
    misc/chmlib \
    misc/dejagnu \
    misc/e2fsprogs-libuuid \
    misc/getopt \
    misc/help2man \
    misc/hicolor-icon-theme \
    misc/hwdata \
    misc/iso-codes \
    misc/pciids \
    misc/py-pexpect@py311 \
    misc/qt5-doc \
    misc/qt5-examples \
    misc/qt5-l10n \
    misc/shared-mime-info \
    misc/sword \
    multimedia/aom \
    multimedia/assimp \
    multimedia/dav1d \
    multimedia/ffmpeg \
    multimedia/gstreamer1 \
    multimedia/gstreamer1-libav \
    multimedia/gstreamer1-plugins \
    multimedia/gstreamer1-plugins-bad \
    multimedia/gstreamer1-plugins-core \
    multimedia/gstreamer1-plugins-dts \
    multimedia/gstreamer1-plugins-dvdread \
    multimedia/gstreamer1-plugins-good \
    multimedia/gstreamer1-plugins-resindvd \
    multimedia/gstreamer1-plugins-theora \
    multimedia/gstreamer1-plugins-ugly \
    multimedia/gstreamer1-qt \
    multimedia/libass \
    multimedia/libdca \
    multimedia/libdvbpsi \
    multimedia/libdvdcss \
    multimedia/libdvdnav \
    multimedia/libdvdread \
    multimedia/libfame \
    multimedia/libmatroska \
    multimedia/libmpeg2 \
    multimedia/libmtp \
    multimedia/libtheora \
    multimedia/libv4l \
    multimedia/libva \
    multimedia/libvdpau \
    multimedia/libvpx \
    multimedia/libx264 \
    multimedia/libxine \
    multimedia/mlt7 \
    multimedia/mlt7-glaxnimate@qt5 \
    multimedia/mlt7-glaxnimate@qt5 \
    multimedia/mlt7-qt@qt5 \
    multimedia/mlt7-qt@qt5 \
    multimedia/mpv \
    multimedia/phonon@qt5 \
    multimedia/pipewire \
    multimedia/qt5-multimedia \
    multimedia/v4l_compat \
    multimedia/vcdimager \
    multimedia/vid.stab \
    multimedia/vlc \
    multimedia/webcamd \
    multimedia/x265 \
    multimedia/xine \
    multimedia/xvid \
    net-im/farstream \
    net-im/libaccounts-glib \
    net-im/libaccounts-qt@qt5 \
    net-im/libnice \
    net-im/libnice-gst1 \
    net-im/libquotient@qt5 \
    net-im/libquotient@qt5 \
    net-im/libsignon-glib@qt5 \
    net-im/loudmouth \
    net-im/qxmpp@qt5 \
    net-im/qxmpp@qt5 \
    net-im/telepathy-logger-qt5 \
    net/arping \
    net/avahi-app \
    net/avahi-autoipd \
    net/avahi-libdns \
    net/glib-networking \
    net/freerdp \
    net/libnet \
    net/liboauth \
    net/libproxy \
    net/libsrtp2 \
    net/libvncserver \
    net/liveMedia \
    net/openldap26-client \
    net/openslp \
    net/p5-IO-Socket-INET6 \
    net/p5-Socket6 \
    net/p5-URI \
    net/pecl-smbclient@php82 \
    net/py-pysocks@py311 \
    net/py-urllib3@py311 \
    net/qoauth-qt5 \
    net/qt5-network \
    net/qt5-networkauth \
    net/rsync \
    net/yaz \
    ports-mgmt/dialog4ports \
    ports-mgmt/packagekit-qt@qt5 \
    ports-mgmt/packagekit-qt@qt5 \
    ports-mgmt/pkg \
    print/cups \
    print/freetype2 \
    print/ghostscript10 \
    print/gsfonts \
    print/harfbuzz \
    print/harfbuzz-icu \
    print/indexinfo \
    print/libijs \
    print/libpaper \
    print/libraqm \
    print/libspectre \
    print/psutils \
    print/qt5-printsupport \
    print/tex-basic-engines \
    print/tex-dvipsk \
    print/tex-formats \
    print/tex-ptexenc \
    print/texinfo \
    print/texlive-base \
    print/texlive-texmf \
    print/texlive-tlmgr \
    print/xpdfopen \
    science/avogadrolibs \
    science/chemical-mime-data \
    science/hdf5 \
    science/libaec \
    science/netcdf \
    science/openbabel \
    security/botan2 \
    security/ca_root_nss \
    security/cracklib \
    security/cyrus-sasl2 \
    security/gnupg \
    security/gnutls \
    security/gpgme \
    security/gpgme-cpp \
    security/gpgme-qt@qt5 \
    security/gpgme-qt@qt5 \
    security/libassuan \
    security/libgcrypt \
    security/libgnome-keyring \
    security/libgpg-error \
    security/libksba \
    security/libotr \
    security/libpwquality \
    security/libsecret \
    security/libsodium \
    security/libssh \
    security/libssh2 \
    security/libtasn1 \
    security/nettle \
    security/nss \
    security/oath-toolkit \
    security/olm \
    security/p11-kit \
    security/p5-Authen-SASL \
    security/p5-Digest-HMAC \
    security/p5-GSSAPI \
    security/p5-IO-Socket-SSL \
    security/p5-Net-SSLeay \
    security/pinentry \
    security/pinentry-tty \
    security/py-bcrypt@py311 \
    security/py-certifi@py311 \
    security/py-cryptography@py311 \
    security/py-ecdsa@py311 \
    security/py-openssl@py311 \
    security/py-paramiko@py311 \
    security/py-pynacl@py311 \
    security/qtkeychain@qt5 \
    security/qtkeychain@qt5 \
    security/rhash \
    security/sudo \
    security/trousers \
    security/xmlsec1 \
    shells/bash \
    sysutils/accountsservice \
    sysutils/bsdisks@qt5 \
    sysutils/cdrtools \
    sysutils/consolekit2 \
    sysutils/coreutils \
    sysutils/dmidecode \
    sysutils/fdupes \
    sysutils/fusefs-libs3 \
    sysutils/gnome_subr \
    sysutils/htop \
    sysutils/libcdio \
    sysutils/libcdio-paranoia \
    sysutils/libdisplay-info \
    sysutils/lscpu \
    sysutils/lsof \
    sysutils/polkit \
    sysutils/polkit-qt-1@qt5 \
    sysutils/py-execnet@py311 \
    sysutils/py-ptyprocess@py311 \
    sysutils/py-scandir@py311 \
    sysutils/qt5-qtdiag \
    sysutils/qt5-qtpaths \
    sysutils/qt5-qtplugininfo \
    sysutils/sg3_utils \
    sysutils/signon-plugin-oauth2@qt5 \
    sysutils/signond@qt5 \
    sysutils/tmux \
    sysutils/zfs-stats \
    textproc/asciidoc \
    textproc/aspell \
    textproc/bat \
    textproc/btparse \
    textproc/catdoc \
    textproc/cmark \
    textproc/discount \
    textproc/docbook \
    textproc/docbook-sgml \
    textproc/docbook-xml \
    textproc/docbook-xsl \
    textproc/ebook-tools \
    textproc/enchant \
    textproc/exempi \
    textproc/expat2 \
    textproc/flex \
    textproc/google-ctemplate \
    textproc/groff \
    textproc/gsed \
    textproc/gtk-doc \
    textproc/highlight \
    textproc/html2text \
    textproc/hunspell \
    textproc/hyphen \
    textproc/ibus \
    textproc/intltool \
    textproc/iso8879 \
    textproc/itstool \
    textproc/jade \
    textproc/jq \
    textproc/libcroco \
    textproc/libcsv \
    textproc/libebml \
    textproc/libkolabxml \
    textproc/libodfgen01 \
    textproc/librevenge \
    textproc/libsass \
    textproc/libvisio01 \
    textproc/libwpd010 \
    textproc/libwps \
    textproc/libxml2 \
    textproc/libxslt \
    textproc/libyaml \
    textproc/minixmlto \
    textproc/p5-Unicode-EastAsianWidth \
    textproc/p5-XML-Parser \
    textproc/p5-YAML-LibYAML \
    textproc/py-CommonMark@py311 \
    textproc/py-alabaster@py311 \
    textproc/py-chardet@py311 \
    textproc/py-docutils@py311 \
    textproc/py-libxml2@py311 \
    textproc/py-m2r@py311 \
    textproc/py-mako@py311 \
    textproc/py-markupsafe@py311 \
    textproc/py-mistune@py311 \
    textproc/py-pygments@py311 \
    textproc/py-pyhamcrest@py311 \
    textproc/py-pypa-docs-theme@py311 \
    textproc/py-pystemmer@py311 \
    textproc/py-python-docs-theme@py311 \
    textproc/py-recommonmark@py311 \
    textproc/py-snowballstemmer@py311 \
    textproc/py-sphinx@py311 \
    textproc/py-sphinx_rtd_theme@py311 \
    textproc/py-sphinxcontrib-websupport@py311 \
    textproc/py-toml@py311 \
    textproc/py-towncrier@py311 \
    textproc/qt5-xml \
    textproc/qt5-xmlpatterns \
    textproc/raptor2 \
    textproc/rasqal \
    textproc/redland \
    textproc/sassc \
    textproc/scim \
    textproc/sdocbook-xml \
    textproc/source-highlight \
    textproc/teckit \
    textproc/texi2html \
    textproc/the_silver_searcher \
    textproc/tinyxml \
    textproc/uchardet \
    textproc/utf8proc \
    textproc/xerces-c3 \
    textproc/xmlcatmgr \
    textproc/xmlcharent \
    textproc/xmlto \
    textproc/zxing-cpp \
    www/kdsoap@qt5 \
    www/kdsoap@qt5 \
    www/libnghttp2 \
    www/neon \
    www/p5-CGI \
    www/p5-HTML-Parser \
    www/p5-HTML-Tagset \
    www/p5-Mozilla-CA \
    www/p5-Template-Toolkit \
    www/p5-URI-Escape-JavaScript \
    www/p5-URI-Escape-XS \
    www/py-beaker@py311 \
    www/py-cheroot@py311  \
    www/py-html5lib@py311 \
    www/py-hyperlink@py311 \
    www/py-requests@py311 \
    www/py-tornado@py311 \
    www/py-wsgidav@py311 \
    www/qt5-webchannel \
    www/qt5-webengine \
    www/qt5-webglplugin \
    www/qt5-websockets \
    www/qt5-websockets-qml \
    www/qt5-webview \
    www/serf \
    www/w3m \
    www/yt-dlp \
    x11-drivers/xf86-input-evdev \
    x11-drivers/xf86-input-keyboard \
    x11-drivers/xf86-input-libinput \
    x11-drivers/xf86-input-mouse \
    x11-drivers/xf86-input-synaptics \
    x11-drivers/xf86-input-wacom \
    x11-fonts/bdftopcf \
    x11-fonts/cantarell-fonts \
    x11-fonts/dejavu \
    x11-fonts/encodings \
    x11-fonts/font-bh-ttf \
    x11-fonts/font-misc-ethiopic \
    x11-fonts/font-misc-meltho \
    x11-fonts/font-util \
    x11-fonts/fontconfig \
    x11-fonts/freefonts \
    x11-fonts/libXfont \
    x11-fonts/libXfont2 \
    x11-fonts/libXft \
    x11-fonts/libfontenc \
    x11-fonts/mkfontscale \
    x11-fonts/noto-basic \
    x11-fonts/source-code-pro-ttf \
    x11-fonts/xorg-fonts-truetype \
    x11-servers/xorg-server@xephyr \
    x11-servers/xorg-server@xorg \
    x11-servers/xorg-server@xvfb \
    x11-servers/xwayland \
    x11-themes/adwaita-icon-theme \
    x11-themes/cursor-dmz-aa-theme \
    x11-themes/cursor-dmz-theme \
    x11-toolkits/gstreamer1-plugins-pango \
    x11-toolkits/gtk20 \
    x11-toolkits/gtk30 \
    x11-toolkits/libXaw \
    x11-toolkits/libXmu \
    x11-toolkits/libXt \
    x11-toolkits/pango \
    x11-toolkits/pangox-compat \
    x11-toolkits/py-tkinter@py311 \
    x11-toolkits/qml-box2d \
    x11-toolkits/qt5-charts \
    x11-toolkits/qt5-datavis3d \
    x11-toolkits/qt5-declarative \
    x11-toolkits/qt5-declarative-test \
    x11-toolkits/qt5-gamepad \
    x11-toolkits/qt5-gui \
    x11-toolkits/qt5-quick3d \
    x11-toolkits/qt5-quickcontrols \
    x11-toolkits/qt5-quickcontrols2 \
    x11-toolkits/qt5-uiplugin \
    x11-toolkits/qt5-virtualkeyboard \
    x11-toolkits/qt5-widgets \
    x11-toolkits/tk86 \
    x11-wm/openbox \
    x11/libICE \
    x11/libSM \
    x11/libX11 \
    x11/libXScrnSaver \
    x11/libXau \
    x11/libXcomposite \
    x11/libXcursor \
    x11/libXdamage \
    x11/libXdmcp \
    x11/libXext \
    x11/libXfixes \
    x11/libXi \
    x11/libXinerama \
    x11/libXpm \
    x11/libXrandr \
    x11/libXrender \
    x11/libXtst \
    x11/libXv \
    x11/libXvMC \
    x11/libXxf86vm \
    x11/libfakekey \
    x11/libinput \
    x11/libwacom \
    x11/libxcb \
    x11/libxcvt \
    x11/libxkbcommon \
    x11/libxkbfile \
    x11/libxshmfence \
    x11/luit \
    x11/pixman \
    x11/plasma-wayland-protocols \
    x11/qt5-qev \
    x11/qt5-x11extras \
    x11/startup-notification \
    x11/xauth \
    x11/xcb-proto \
    x11/xcb-util \
    x11/xcb-util-cursor \
    x11/xcb-util-errors \
    x11/xcb-util-image \
    x11/xcb-util-keysyms \
    x11/xcb-util-renderutil \
    x11/xcb-util-wm \
    x11/xcb-util-xrm \
    x11/xcursorgen \
    x11/xdotool \
    x11/xdpyinfo \
    x11/xkbcomp \
    x11/xkeyboard-config \
    x11/xorgproto \
    x11/xprop \
    x11/xset \
    x11/xterm \
    x11/xtrans \
    x11/xwd \
    && pkg clean -a -y

# For D-Bus to be willing to start it needs a Machine ID
RUN dbus-uuidgen > /etc/machine-id && chmod 444 /etc/machine-id

# All sorts of things break horribly without a default timezone being set
RUN cp /usr/share/zoneinfo/UTC /etc/localtime && chmod 444 /etc/localtime

# Create a user to run operations under
RUN pw useradd -c "Gitlab CI Operator" -n user -m -s /usr/local/bin/bash && chmod 755 /home

# Switch to our unprivileged account
USER user

# Ensure we have a UTF-8 locale
ENV LANG=en_US.UTF-8
